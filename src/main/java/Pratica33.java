import utfpr.ct.dainf.if62c.pratica.Matriz;

/**
 * IF62C Fundamentos de Programação 2
 * Exemplo de programação em Java.
 * @author Wilson Horstmeyer Bogado <wilson@utfpr.edu.br>
 */
public class Pratica33 {

    public static void main(String[] args) {
        Matriz orig = new Matriz(3, 2);
        double[][] m = orig.getMatriz();
        m[0][0] = 0.0;
        m[0][1] = 0.1;
        m[1][0] = 1.0;
        m[1][1] = 1.1;
        m[2][0] = 2.0;
        m[2][1] = 2.1;
        Matriz transp = orig.getTransposta();
        System.out.println("Matriz original: " + orig);
        System.out.println("Matriz transposta: " + transp);
        
        //Matriz soma_matriz =orig.soma(transp);//Aqui iremos somar a matriz original com sua transposta.
        System.out.println("Soma das matrizes: " + orig.soma(orig));//Exibimos a soma das matrizes.
        
        Matriz prod_matriz = orig.prod(transp);
        System.out.println("Produto das matrizes: " + prod_matriz);
    }
}
